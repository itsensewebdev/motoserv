<?php

$lang['cal_su']			= "Su";
$lang['cal_mo']			= "Mo";
$lang['cal_tu']			= "Tu";
$lang['cal_we']			= "We";
$lang['cal_th']			= "Th";
$lang['cal_fr']			= "Fr";
$lang['cal_sa']			= "Sa";
$lang['cal_sun']		= "Sun";
$lang['cal_mon']		= "Mon";
$lang['cal_tue']		= "Tue";
$lang['cal_wed']		= "Wed";
$lang['cal_thu']		= "Thu";
$lang['cal_fri']		= "Fri";
$lang['cal_sat']		= "Sat";
$lang['cal_sunday']		= "Niedziela";
$lang['cal_monday']		= "Poniedziałek";
$lang['cal_tuesday']	= "Wtorek";
$lang['cal_wednesday']	= "Środa";
$lang['cal_thursday']	= "Czwartek";
$lang['cal_friday']		= "Piątek";
$lang['cal_saturday']	= "Sobota";
$lang['cal_jan']		= "Jan";
$lang['cal_feb']		= "Feb";
$lang['cal_mar']		= "Mar";
$lang['cal_apr']		= "Apr";
$lang['cal_may']		= "May";
$lang['cal_jun']		= "Jun";
$lang['cal_jul']		= "Jul";
$lang['cal_aug']		= "Aug";
$lang['cal_sep']		= "Sep";
$lang['cal_oct']		= "Oct";
$lang['cal_nov']		= "Nov";
$lang['cal_dec']		= "Dec";
$lang['cal_january']	= "Styczeń";
$lang['cal_february']	= "Luty";
$lang['cal_march']		= "Marzec";
$lang['cal_april']		= "Kwiecień";
$lang['cal_mayl']		= "Maj";
$lang['cal_june']		= "Czerwiec";
$lang['cal_july']		= "Lipiec";
$lang['cal_august']		= "Sierpień";
$lang['cal_september']	= "Wrzesień";
$lang['cal_october']	= "Październik";
$lang['cal_november']	= "Listopad";
$lang['cal_december']	= "Grudzień";


/* End of file calendar_lang.php */
/* Location: ./system/language/english/calendar_lang.php */