<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

 

class Calendar extends CI_Controller
{
	// THE CONSTRUCTOR //
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Gestione_model');
        $this->load->model('Impostazioni_model');
        $this->lang->load('global', $this->Impostazioni_model->get_lingua());
        $this->lang->load('guest', $this->Impostazioni_model->get_lingua()); 
        $this->load->helper('language');
        $this->load->helper('download');
        $this->load->library('zip');
        $this->Impostazioni_model->gen_token();
    }

	// PRINT A CUSTOMERS PAGE //
    public function index()
    {
 
$this->display();
	}
	
	
function display($year= null, $month = null){

if (!$year) {$year = date("Y");}
if (!$month) {$month= date("m");}

	         $data['impostazioni'] = $this->Impostazioni_model->lista_impostazioni();
			 $data['lista'] = $this->Gestione_model->lista_oggetti();
            $data['n_ordini'] = $this->Gestione_model->conta_ordini();
            $data['n_riparazioni'] = $this->Gestione_model->conta_riparazioni();
            $data['n_clienti'] = $this->Gestione_model->conta_clienti();
            $data['lista_c'] = $this->Gestione_model->lista_clienti();
			
	              $data['workers_c'] = $this->Gestione_model->lista_workers();
				  
			$data['n_insertoni'] = $this->Gestione_model->conta_insertoni();
  $this->load->view('header', $data);
  $this->load->view('footer', $data);




$this->load->model('Mycal_model');

   
$datacze['calendar'] = $this->Mycal_model->generate($year, $month);

 $this->load->view('calendar', $datacze);
 

}

    }
	
	




 
