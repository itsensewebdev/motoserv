<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Financial
 *
 *
 * @package		FixBook
 * @category	Controller
 * @author		Michal Gorecki / Luigi Vento
*/

class Reports extends CI_Controller
{
	// THE CONSTRUCTOR //
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('Gestione_model');
        $this->load->model('Impostazioni_model');
        $this->load->model('Reports_model');
        $this->load->helper('language');
        $this->lang->load('global', $this->Impostazioni_model->get_lingua());
    }

	// PRINT A FINANCIAL PAGE //
    public function index()
    {
        $data['impostazioni'] = $this->Impostazioni_model->lista_impostazioni();
     
            $data['all_insertoni'] = $this->Gestione_model->conta_all_insertoni();
     
		
	
		
        if ($this->session->userdata('LoggedIn')) {
            $data['lista'] = $this->Gestione_model->lista_guadagni(date('m'), date('Y'));
            $this->load->view('reports_page', $data);
        } else {
            $this->load->view('login_page', $data);
        }
    }
 
	
	
    // GET AN ORDER/REPARATION FOR SHOW IT //
    public function preparereport()
    {
        $month = $this->input->post('date', true);
        $data = $this->Reports_model->getdatareport($month);
        echo json_encode($data);
    }

	
	
	
	// PRINT A FINANCIAL GRAPH FOR MONTHS AND YEARS //
    public function data($mese, $anno)
    {
        $data['valuta'] = $this->Impostazioni_model->get_currency();
        $data['impostazioni'] = $this->Impostazioni_model->lista_impostazioni();
        if ($this->session->userdata('LoggedIn')) {
            if (isset($mese) && isset($anno)) {
                $data['lista'] = $this->Gestione_model->lista_guadagni($mese, $anno);
            } else {
                $data['lista'] = $this->Gestione_model->lista_guadagni(date('m'), date('Y'));
            }
            $this->load->view('reports_page', $data);
        } else {
            $this->load->view('login_page');
        }
    }
 
	
	
}

/* End of file report.php */
/* Location: ./system/application/controllers/report.php */
