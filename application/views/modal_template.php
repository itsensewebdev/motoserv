
<div class="col-md-8 modal fade" id="obj" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin:0 auto">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="titf">Nowe zlecenie</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <p class="tips custip"></p>
                    <div class="row">
                        <form>
                            <div class=" col-sm-6">
                                <div class="form-group">
                                    <label>
                                       Klient
                                    </label>
                                    <div class="row">
                                        <div class="col-lg-10 col-sm-11 col-xs-10">
                                            <div class="iconic-input"><i class="fa fa-user" style="right: 20px; right: 30px; top: -1px; z-index: 200;"></i>
                                                <select id="nominativo1" data-num="1" class="form-control m-bot15" style="width: 100%">

                                                    <?php 
foreach ($lista_c as $ogg) :
echo '<option value="'.$ogg['id'].'">'.$ogg['nome'].' '.$ogg['cognome'].'</option>';
endforeach; 
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-sm-1 col-xs-2">
                                            <a class="add_c btn"><i class="fa fa-user-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                                   
                               <div class="col-sm-3">
                                <div class="form-group">
                                    <label>
                                        Marka pojazdu  
                                    </label>
                                    <div class="iconic-input"><i class="fa fa-car"></i>
                                        <input id="modello1" type="text" class="validate form-control"  rows="6">
                                    </div>
                                </div>
                            </div>
                            
                                   <div class=" col-sm-3">
                                <div class="form-group">
                                    <label>
                                        Model pojazdu
                                    </label>
                                    <div class="iconic-input"><i class="fa fa-car"></i>
                                        <input id="anticipo1" type="text" class="validate form-control">
                                    </div>
                                </div>
                            </div>
                            
                                           <div class="col-sm-6">
                                <div class="form-group">
                                    <label>
                                        Data i godzina przyjęcia pojazdu                                    </label>
                                 
                                    <div class="iconic-input inp_cat1"><i class="fa fa-calendar"></i>
                                        <input id="categoria1" type="text" class="validate form-control"  >
                                        <script>
                                        $('#categoria1').datetimepicker({

 autoclose: true,

todayHighlight: true,
daysOfWeekHighlighted: [0,6],
weekStart:1,
 language: 'pl',
  stepping: 15


}); 



                                                    </script>
                                        
                                    </div>
                                </div>
                            </div>
                            


                             
                             
                            
                    
                     
            
                         
                            
                                            <div class=" col-sm-3">
                                <div class="form-group">
                                    <label>
                                        Rok produkcji
                                    </label>
                                    <div class="iconic-input"><i class="fa fa-clock-o"></i>
                                        <input id="rokprod1" type="text"   class="validate form-control">
                                    </div>
                                </div>
                            </div>
                        
                            
                            
                         
                            <div class=" col-sm-3">
                                <div class="form-group">
                                    <label>
                                        Pojemność
                                    </label>
                                    <div class="iconic-input"><i class="fa fa-eur"></i>
                                        <input id="prezzo1" type="text" class="validate form-control">
                                    </div>
                                </div>   </div>

<input id="gotsms1" type="hidden" value="0">



    <div class="col-lg-6 col-sm-12">
                                <div class="form-groupa">
                                    <label>
                                        Opis usterek
                                    </label>
                                    <div class="iconic-input"><i class="fa  fa-chain-broken"></i>
                                    
                                           <textarea id="opis_usterek1" type="text" class="validate form-control" rows="3"></textarea>
                                    </div>

                                    <div style="background-color:#eeeeee; z-index:300; margin-top:20px; padding:5px; ">
                                      <label>
                                       <div > Przydzielanie zadań</div>
                                    </label>
                                    <div class="input_fields_wrap" style="  margin-bottom:20px">
                        
                                 
                                    </div>
                                    <div><button class="add_field_button btn btn-success">Dodaj nowe zadanie</button></div>
                                    </div>
                                  

<script>
    var datasza = <?php echo json_encode($workers_c); ?> //Don't forget the extra semicolon!
</script>

                                    
                                </div>
                            </div>






                                
                                
                                             <div class=" col-sm-3">
                                <div class="form-group">
                                    <label>
                                        Moc
                                    </label>
                                    <div class="iconic-input"><i class="fa fa-cogs"></i>
                                        <input id="moc1" type="text"   class="validate form-control">
                                    </div>
                                </div>
                            </div>
                                  <div class=" col-sm-3">
                                <div class="form-group">
                                    <label>
                                        Nr Rej
                                    </label>
                                    <div class="iconic-input"><i class="fa fa-barcode"></i>
                                        <input id="rej1" type="text"   class="validate form-control">
                                    </div>
                                </div>
                            </div>


                                             <div class=" col-sm-6">
                                <div class="form-group">
                                    <label>
                                        VIN
                                    </label>
                                    <div class="iconic-input"><i class="fa fa-id-card-o"></i>
                                        <input id="vin1" type="text"   class="validate form-control">
                                    </div>
                                </div>
                            </div>
                               
                               <div class=" col-sm-6">
                                <div class="form-group">
                                    <label>
                                        Olej w silniku
                                    </label>
                                    <div class="iconic-input"><i class="fa fa-tint"></i>
                                        <input id="oil" type="text"   class="validate form-control">
                                    </div>
                                </div>
                            </div>

                   
                                         <div class="col-lg-12 col-sm-12"> 
                                <div class="form-group">
                                    <label>
                                        Komentarz
                                    </label>
                                    <div class="iconic-input"><i class="fa  fa-chain-broken"></i>
                                        <textarea id="commenti1" type="text" class="validate form-control" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                          
                       
                          
                      

                           
                            <div style="clear: both;"></div>
                          
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer" id="footerOR1"> </div>
        </div>
    </div>
</div>


<div class="col-md-8 modal fade" id="visualizza_ordini" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin:0 auto">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><div id="titoloOEa">Podsumowanie zlecenia</div></h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <div class="row">
                     
                        <div class="col-md-6  bio-row">
                            <p><span class="bold"><i class="fa fa-user"></i> Klient </span><span id="clientec"></span></p>
                        </div>
                        <div class="col-md-6 bio-row">
                            <p><span class="bold"><i class="fa fa-barcode"></i> Numer zlecenia </span><span id="codicec"></span></p>
                        </div>
                        
                         
             
                           <div class="col-md-6 bio-row">
                            <p><span class="bold"><i class="fa fa-tag"></i> Pojazd </span><span id="modelloc"></span></p>
                        </div>
                     
                  <div class="col-md-6 bio-row">
                            <p><span class="bold"><i class="fa fa-tag"></i> Model pojazdu </span><span id="anticipoc"></span></p>
                        </div>
                     
                        
                          <div class="col-md-6 bio-row">
                            <p><span class="bold"><i class="fa fa-calendar"></i> Data przyjęcia </span><span id="dataAperturac"></span></p>
                        </div>
                         
                          <div class="col-md-6 bio-row">
                            <p><span class="bold"><i class="fa fa-calendar"></i> Rok produkcji </span><span id="rokprodc"></span></p>
                        </div>
                         
                          <div class="col-md-6 bio-row">
                            <p><span class="bold"><i class="fa fa-calendar"></i> Numer VIN </span><span id="vin1c"></span></p>
                        </div>
                         
                         
                          <div class="col-md-6 bio-row">
                            <p><span class="bold"><i class="fa fa-calendar"></i> Rejestracja </span><span id="rejestracja1c"></span></p>
                        </div>
                         
                         
                     
                        <div class="col-md-6   bio-row stato">
                            <p><span class="bold"><i class="fa fa-signal"></i> Status zlecenia </span><span id="statoc"></span></p>
                   </div>
                       <div class="col-md-6   bio-row">
                            <p><span class="bold"><i class="fa fa-phone"></i> Telefon </span><span id="telefonoc"></span></p>
                        </div>
                        
                     
                        <div class="col-md-12 bio-row">
                            <p><span class="bold"><i class="fa fa-chain-broken"></i> Usterki / Wymiana</span><span id="opis_usterek2"></span></p>
                        </div>    
                      
                        
                        
                        
                         
           
                       
                   
                        
                
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6 bio-row fastsms">
                                    <div class="form-group commenti">
                                        <label>
                                            <?= $this->lang->line('Fastsms_t');?>
                                        </label>
                                        <textarea class="form-control" id="fastsms" rows="6" placeholder="Treść wiadomości"></textarea>
                                        <button type="button" id="sendsmsfast"><i class="fa fa-check"></i> Wyślij</button>
                                    </div>
                                </div>
                                <div class="col-md-6 bio-row textareacom">
                                    <div class="form-group commenti">
                                        <label>
                                            Komentarz do zlecenia
                                        </label>
                                        <textarea class="form-control" id="commentic" rows="6" disabled=""></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                       </div>
                </div>
                  <div id="footerOR" class="modal-footer">

            </div>
            </div>
          
        </div>
    </div>
</div>


<div class="modal fade" id="visualizza_clienti" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><div id="titoloclienti"></div></h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <a data-dismiss="modal" data-dismiss="modal" href="#obj" data-toggle="modal" data-id="" class="flatb add">
                                <i class="fa fa-plus-circle"></i> <?=$this->lang->line('a_ordine');?>
                            </a>
                        </div>
                        <div class="col-md-12 col-lg-6 bio-row">
                            <a data-dismiss="modal" data-dismiss="modal" href="#lista_del_cliente" data-toggle="modal" class="flatb lista">
                                <i class="fa fa-list"></i> <?=$this->lang->line('lista_ordini');?>
                            </a>
                        </div>
                        <div class="col-md-12 col-lg-6 bio-row">
                            <p><span class="bold"><i class="fa fa-user"></i> <?=$this->lang->line('nome');?> </span><span id="nomec"></span></p>
                        </div>
                        <div class="col-md-12 col-lg-6 bio-row">
                            <p><span class="bold"><i class="fa fa-user"></i> <?=$this->lang->line('cognome');?> </span><span id="cognomec"></span></p>
                        </div>
                        <div class="col-md-12 col-lg-6 bio-row">
                            <p><span class="bold"><i class="fa fa-road"></i> <?=$this->lang->line('indirizzo');?></span><span id="indirizzoc"></span></p>
                        </div>
                        <div class="col-md-12 col-lg-6 bio-row">
                            <p><span class="bold"><i class="fa fa-globe"></i> <?=$this->lang->line('citta');?></span><span id="cittac"></span></p>
                        </div>
                        <div class="col-md-12 col-lg-6 bio-row">
                            <p><span class="bold"><i class="fa fa-phone"></i> <?=$this->lang->line('Telefono_t');?> </span><span id="telefonocc"></span></p>
                        </div>
                        <div class="col-md-12 col-lg-6 bio-row">
                            <p><span class="bold"><i class="fa fa-envelope"></i> <?=$this->lang->line('email');?> </span><span id="emailc"></span></p>
                        </div>
                        <div class="col-md-12 col-lg-6 bio-row">
                            <p><span class="bold"><i class="fa fa-barcode"></i> Nazwa firmy </span><span id="firma1"></span></p>
                        </div>   
						
						<div class="col-md-12 col-lg-6 bio-row">
                            <p><span class="bold"><i class="fa fa-barcode"></i> <?=$this->lang->line('vat');?> </span><span id="vatc"></span></p>
                        </div>
                        <div class="col-md-12 col-lg-6 bio-row">
                            <p><span class="bold"><i class="fa fa-quote-left"></i> <?=$this->lang->line('cf');?> </span><span id="cfc"></span></p>
                        </div>

                    </div>
                    <div class="form-group commenti">
                        <label><?=$this->lang->line('Commenti_t');?></label>
                        <textarea class="form-control" id="commentiu" rows="6" disabled></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer" id="footerClienti"></div>
        </div>
    </div>
</div>


<div class="modal fade" id="lista_del_cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="tit_ordini_cliente"></h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <table class="display compact table table-bordered table-striped" id="clienti_table">
                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                               <?= $this->lang->line('Stato_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Cliente_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Tipo_t');?>
                                </th>
                                <th id="modello_ht">
                                    <?= $this->lang->line('Modello_t');?>
                                </th>
                                <th id="guasto_ht">
                                    <?= $this->lang->line('Guasto_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Data_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Telefono_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Status_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Azioni_t');?>
                                </th>
                            </tr>
                        </thead>

                        <tfoot>
                            <tr>
                                <th>
                                    <?= $this->lang->line('ID_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Stato_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Cliente_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Tipo_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Modello_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Guasto_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Data_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Telefono_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Status_t');?>
                                </th>
                                <th>
                                    <?= $this->lang->line('Azioni_t');?>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer" id="footlista"><div class="btn-group btn-group-justified left"><button data-dismiss="modal" class="btn btn-default" type="button"><i class="fa fa-reply"></i> <?= $this->lang->line('js_torna_indietro');?></button></div></div>
            </div>
        </div>
    </div>
</div>


<!-- ============= MODAL MODIFICA CLIENTI ============= -->
<div class="col-md-8 modal fade" id="clientimodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin:0 auto">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="titclientid">Nowy klient</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <p class="tips custip"></p>
                    <div class="row">
                        <form class="col s12">
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?=$this->lang->line('nome');?></label>
                                    <div class="iconic-input"><i class="fa  fa-user"></i>
                                        <input id="nome1" type="text" class="validate form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?=$this->lang->line('cognome');?></label>
                                    <div class="iconic-input"><i class="fa  fa-user"></i>
                                        <input id="cognome1" type="text" class="validate form-control" required>
                                    </div>
                                </div>
                            </div>
							
							             <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?=$this->lang->line('Telefono_t');?></label>
                                    <div class="iconic-input"><i class="fa fa-phone"></i>
                                        <input id="telefono1" type="text" class="validate form-control" data-mask="(999) 999-9999">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?=$this->lang->line('email');?></label>
                                    <div class="iconic-input"><i class="fa fa-envelope"></i>
                                        <input id="email1" type="email" class="validate form-control">
                                    </div>
                                </div>
                            </div>
							
						
						  <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label>Nazwa firmy</label>
                                    <div class="iconic-input"><i class="fa fa-envelope"></i>
                                        <input id="firma2" class="validate form-control">
                                    </div>
                                </div>
                            </div>    	
						
							
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?=$this->lang->line('citta');?></label>
                                    <div class="iconic-input"><i class="fa fa-globe"></i>
                                        <input id="citta1" type="text" class="validate form-control">
                                    </div>
                                </div>
                            </div>
               
                          
                            <div class="col-lg-6 col-sm-12">
                                <div class="form-group">
                                    <label><?=$this->lang->line('indirizzo');?></label>
                                    <div class="iconic-input"><i class="fa fa-road"></i>
                                        <input id="indirizzo1" type="text" class="validate form-control">
                                    </div>
                                </div>
                            </div>

							<div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?=$this->lang->line('vat');?></label>
                                    <div class="iconic-input"><i class="fa fa-envelope"></i>
                                        <input id="vat1" class="validate form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 input-field">
                                <div class="form-group">
                                    <label><?=$this->lang->line('cf');?></label>
                                    <input id="cf1"  class="validate form-control"></textarea>
                            </div>
                            </div>
                        <div class="input-field col-lg-12">
                            <div class="form-group">
                                <label><?=$this->lang->line('Commenti_t');?></label>
                                <textarea class="form-control" id="commentiu1" rows="6"></textarea>
                            </div>
                        </div>
                        </form>
                </div>
            </div>
            <div class="modal-footer" id="footerClienti1"> </div>
        </div>
    </div>
</div>
</div>