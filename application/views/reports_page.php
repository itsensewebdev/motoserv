<?php require 'header.php'; ?>
<script type="text/javascript"> 
	window.base_url = "<?=site_url(); ?>";
</script>
 	<link rel="stylesheet" href="<?=site_url('assets/bootstrap-datepicker/css/datepicker.css'); ?>" />
	
	
		    <script src="<?=site_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>
	<script src="<?=site_url('js/advanced-form-components.js'); ?>"></script>
	
	
<script type="text/javascript" src="<?=site_url().'home/js/reports'; ?>"></script>
<script type="text/javascript" src="<?=site_url().'home/js/canvas'; ?>"></script>
 
	<!--main content start-->  
	<section id="main-content"> 
	
	
	
				<div class="col-lg-12">
					<section class="panel">
						<header class="panel-heading">
							<?=$this->lang->line('resoconto_entrate_mese');?>: <i><?=$lista[32].'/'.$lista[33]; ?></i>
				           
                     
						</header>
						<div class="panel-body">
							<div id="hero-area" class="graph">
								
		<div id="chartContainer" style="height: 400px; width: 100%;"></div>
		
							</div>
						</div>
					</section>
				</div>
	
			<div class="col-lg-8 col-sm-6">
					<section class="panel">
						<header class="panel-heading">
							<?=$this->lang->line('scegli_mese_visionare');?>
						</header>
						<div class="form-group panel-body date_pc">
							<div data-date-minviewmode="months" data-date-viewmode="years" data-date-format="mm/yyyy" data-date="" data-date-language="pl-PL" class="input-append date dpMonths">
								<input type="text"  value="<?=$lista[32].'/'.$lista[33]; ?>" id="data_salto" size="16" class="form-control">						                            
								<span class="input-group-btn add-on">
                                        	<button class="btn btn-danger" type="button"><i class="fa fa-calendar"></i></button>
							</span>
							</div>
							<button type="button" class="finanze_subdate btn btn-info "><i class="fa fa-refresh"></i>
								<?=$this->lang->line('aggiorna');?>
							</button>
						</div>
					</section>
				</div>
				<div class="col-lg-4 col-sm-6 state-overview">
					<section class="panel">
						<div class="symbol blue">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="value" >
							<h1>
                            <?php echo $all_insertoni; ?>
				            </h1>
							<p>
								Wszystkich zleceń w systemie						</p>
						</div>
					</section>
				</div>
		
	
	
 
 
	
	
	</section>
	<!--main content end-->
	
	<script>
	
		jQuery(document).ready(function ()
							   {

			jQuery('.finanze_subdate').click(function (e)
											 {
				var date = jQuery("#data_salto").val();
		 
		 
		   jQuery.ajax({
            type: "POST",
            url: base_url + "reports/preparereport",
            data: "date=" + date,
            cache: false,
	 
            success: function(result) {
			
		 
		var obj = jQuery.parseJSON(result);
            console.log(obj);  

//graph

			var chart = new CanvasJS.Chart("chartContainer",
			{
				title:{
					text: "Zrealizaowane zadania"
				},

				data: [
				{
					type: "bar",
 
			 
					dataPoints:obj
 
				
				}				
				]
			});
	chart.render();
		
//end graph
	 
					}
					});
		 
		 
		 
			});

		});

	</script>
	 




	<?php require 'footer.php'; ?>