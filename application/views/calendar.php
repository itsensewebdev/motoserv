<?php  
?>
<script type="text/javascript">
    window.base_url = "<?= site_url(); ?>";
</script>

 <style type="text/css">
.calendar {
	font-family: Arial, Verdana, Sans-serif;
	width: 100%;
	min-width: 960px;
	border-collapse: collapse;
}

.calendar tbody tr:first-child th {
	color: #505050;
	margin: 0 0 1px 0;
}

.day_header {
	font-weight: normal;
	text-align: center;
	color: #757575;
	font-size: 11px;
}

.calendar th {
    
    height: 50px;
   
}


.calendar td {
	width: 14%; /* Force all cells to be about the same width regardless of content */
	border:1px solid #29a6de;
	height: 100px;
	vertical-align: top;
	font-size: 10px;
	padding: 0;
}

.calendar td:hover {
 
}

.day_listing {
	display: block;
	text-align: right;
	font-size: 12px;
	color: #2C2C2C;
	padding: 0;
}

.today {
	background: #fefefe;
	height: 100%;
	width:100%;
	padding-left:5px;
	padding-right:5px; 
 
	 
}
</style>

<link rel="stylesheet" href="<?= site_url('assets/data-tables/DT_bootstrap.css'); ?>" />
<script type="text/javascript" src="<?=site_url().'home/js/validate'; ?>"></script>
<script type="text/javascript" src="<?=site_url().'home/js/home'; ?>"></script>
<script type="text/javascript" src="<?=site_url().'home/js/clienti'; ?>"></script>

<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">

        <!-- page start-->
        <!--state overview start-->
   
        <!--state overview end-->
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Kalendarz
                    <div style="float:right" style="color:#A9D86E"><a href="#obj" style="color:#fff; text-decoration:none" data-toggle="modal" data-tipo="2" class="add">Nowe zlecenie <i class="fa fa-plus-circle" aria-hidden="true"></i></a></div>
                </header>

                <div class="panel-body"  >
                    
<?php


echo $calendar;

?>

<script type="text/javascript">
$(document).ready(function(){

$('.calendar .day').click(function(){
day_num = $(this).find('.day_num').html();

if (day_num) {
 
}
});


});
</script>


                </div>
            </section>
        </div>

    <?php include('modal_template.php'); ?>
        
<!-- page end-->
</section>
</section>
<!--main content end-->

<link rel="stylesheet" href="<?= site_url('assets/css/toastr.min.css'); ?>">
<script><?php include(FCPATH.'assets/js/toastr.min.js'); ?></script>
 
 <script type="text/javascript"><?php include(FCPATH.'assets/advanced-datatable/media/js/jquery.dataTables.min.js'); ?></script>
 
<script type="text/javascript"><?php include(FCPATH.'assets/advanced-datatable/media/js/dataTables.responsive.js'); ?></script>
 
<script type="text/javascript"><?php include(FCPATH.'assets/data-tables/DT_bootstrap.js'); ?></script>
<script type="text/javascript"><?php include(FCPATH.'assets/js/drag_scroll.js'); ?></script>




 