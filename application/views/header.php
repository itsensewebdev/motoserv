	<!DOCTYPE html>

    <?php if( $impostazioni[0]['rtl_support'] ) echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ar" dir="rtl">';
    else echo '<html lang="en">'; ?>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Luigi Verzì">
		<link rel="shortcut icon" href="img/favicon.png">

		<title><?=$impostazioni[0]['titolo']; ?> - <?=$this->lang->line('pannello_dc_w');?></title>


	<link href="<?=site_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
		<link href="<?=site_url('css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 
  	 <script><?=include(FCPATH.'js/jquery-ui.js'); ?></script>

  	 	<script><?=include(FCPATH.'js/bootstrap.min.js'); ?></script>

		<script><?=include(FCPATH.'js/bootstrap-datetimepicker.min.js'); ?></script>
		 <script><?=include(FCPATH.'js/locales/bootstrap-datetimepicker.pl.js'); ?></script>
	
		<link href="<?=site_url('css/hover.css'); ?>" rel="stylesheet">
		<link href="<?=site_url('css/bootstrap-reset.css'); ?>" rel="stylesheet">
		<link href="<?=site_url('assets/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
		<link href="<?=site_url('assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css'); ?>" rel="stylesheet" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?=site_url('css/owl.carousel.css'); ?>" type="text/css">
        <link rel="stylesheet" href="<?= site_url('assets/advanced-datatable/media/css/dataTables.responsive.css'); ?>" />
		<link href="<?=site_url('css/style.css'); ?>" rel="stylesheet">
        
        <?php // ADD THE RTL SUPPORT IF CHECKED
        if( $impostazioni[0]['rtl_support'] ) echo '<link href="'.site_url('css/rtl.css').'" rel="stylesheet">'; 
        ?>
		<link href="<?=site_url('css/style-responsive.css'); ?>" rel="stylesheet" />
        <script><?=include(FCPATH.'assets/js/pace.min.js');?></script>
 
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
		<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	</head>

                
    <?php 
        $colore = $impostazioni[0]['colore_prim'];
        $alfa = $this->Impostazioni_model->hex2rgba($colore, 0.05);
        echo '<style id="colori">';
        include 'js/colori_js.php';
        echo '</style>';
    ?>
    <script>
        jQuery(document).ready(function () {
            $("#black").fadeOut(500);
        });
    </script>

	<body>
        <?php if(!$impostazioni[0]['background_transition']) { ?><div id="black"></div><?php } ?>
		<section id="container">
			<!--header start-->
			<header class="header white-bg">
				<div class="sidebar-toggle-box">
					<div class="fa fa-bars tooltips" data-placement="right" data-original-title="Zmień widok"></div>
				</div>
				<!--logo start-->
				<a href="<?=site_url(''); ?>" class="logo">
					<p><span><?=$this->lang->line('pannello_dc');?></span></p>
                    <p><?=rawurldecode($impostazioni[0]['titolo']); ?></p>
				</a>
				<!--logo end-->

			
			</header>
			<!--header end-->
			<!--sidebar start-->
			<aside>
				<div id="sidebar" class="nav-collapse ">
					<!-- sidebar menu start-->
					<ul class="sidebar-menu" id="nav-accordion">
                        <li class="nav_title"><img src="<?= ($impostazioni[0]['logo'] == 'default') ? 'http://fixbook.it/img/logo_nav.png' : site_url('img').'/'.$impostazioni[0]['logo']; ?>"></li>
						<li>
                            <a class="hvr-bounce-to-right <?php if ('http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] == site_url('')) {
    echo 'active';
    } ?>" href="<?=site_url(''); ?>">
								<i class="fa fa-car"></i>
								<span>Zlecenia</span>
							</a>
						</li>
	<li>
							<a class="hvr-bounce-to-right <?php if ('http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] == site_url('calendar/')) {
    echo 'active';
} ?>" href="<?=site_url('calendar//'); ?>">
								<i class="fa fa-calendar-check-o"></i>
								<span><?=$this->lang->line('calendar');?></span>
							</a>
						</li>
						<li>
                            <a class="hvr-bounce-to-right <?php if ('http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] == site_url('clienti/')) {
    echo 'active';
    } ?>" href="<?=site_url('clienti/'); ?>">
								<i class="fa fa-user"></i>
								<span><?=$this->lang->line('clientis');?></span>
							</a>
						</li>
					

					<li>
                            <a class="hvr-bounce-to-right <?php if ('http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] == site_url('reports/')) {     echo 'active';     } ?>" href="<?=site_url('reports/'); ?>">
								<i class="fa fa-bar-chart-o"></i>
								<span>Raporty</span>
							</a>
						</li>
					<!--

					<li>
							<a class="hvr-bounce-to-right <?php //if ('http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] == site_url('impostazioni/')) {
    //echo 'active';} ?>" href="<?//=site_url('impostazioni//'); ?>">
								<i class="fa fa-cog"></i>
								<span>Ustawienia</span>
							</a>
						</li>
				-->		
						
						
						
                        <li class="logouts">
                            <a class="hvr-bounce-to-right" href="<?=site_url('login/logout'); ?>">
                                <i class="fa fa-key"></i>
								<span>Wyloguj się</span>
							</a>
						</li>

					</ul>
					<!-- sidebar menu end-->
				</div>
			</aside>
			<!--sidebar end-->