<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}



class Mycal_model extends CI_Model {



 function generate($year,$month) {

	 $conf = array (
	       'month_type'   => 'long',
               'day_type'     => 'long',
	'start_day' =>'monday',
   'show_next_prev' => TRUE,
   'next_prev_url' => base_url().'calendar/display'
   
   );
   
   
   $conf['template'] = '

   {table_open}<table border="0" cellpadding="0" cellspacing="0" class="calendar">{/table_open}

   {heading_row_start}<tr style=" ">{/heading_row_start}

   {heading_previous_cell}<th style="height:30px "><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
   {heading_title_cell}<th style="text-align:center" colspan="{colspan}">{heading}</th>{/heading_title_cell}
   {heading_next_cell}<th style="text-align:right"><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}

   {heading_row_end}</tr>{/heading_row_end}

   {week_row_start}<tr>{/week_row_start}
   {week_day_cell} <td style="background-color:#29a6de;color:#fff; font-size:14px;vertical-align:middle; text-align:center">{week_day}</td>{/week_day_cell}
   {week_row_end}</tr>{/week_row_end}

   {cal_row_start}<tr class="days">{/cal_row_start}
   {cal_cell_start}<td {/cal_cell_start}

   {cal_cell_content}class="day">

   <div class="day_num" style="font-size:10px">{day}</div>

   <div class="content" style="font-size:11px">{content}</div>{/cal_cell_content}
   {cal_cell_content_today}class="day" style="padding:0px 0px; margin:0; background-color:#aedcf0"><div class="day_num today highlight" style="font-size:10px" ><div class="day_num" >{day} <div class="content" style="font-size:11px">{content}</div>{/cal_cell_content_today}

   {cal_cell_no_content}class="day"><div class="day_num" style="font-size:10px">{day} </div>{/cal_cell_no_content}
   {cal_cell_no_content_today}class="day " style="padding:0px 0px; margin:0;background-color:#aedcf0"><div class="day_num highlight" style="font-size:10px">{day} <br><br>{/cal_cell_no_content_today}

   {cal_cell_blank}&nbsp;{/cal_cell_blank}

   {cal_cell_end}</td>{/cal_cell_end}
   {cal_row_end}</tr>{/cal_row_end}

   {table_close}</table>{/table_close}
';
   

    $events  = $this -> get_calendar_data($year,$month);

 

$this->load->library('calendar', $conf);


 return  $this->calendar->generate($year,$month,$events);

}



  

   function get_calendar_data($year,$month){
   
   

	
	 $temp = '';
      $events = array();
	  //zapytanie do bazy. wazne jest sortowanie po dacie aby ułożyły się jedna pod drugim z tą samą datą ze względu na zapamietany poprzedni wiersz w zmiennej $temp
	  
      $query = $this->db->select('dataApertura , Modello, status, ID')->from('oggetti')->where('status !=' , "0")->like('dataApertura',"$year-$month")->order_by("dataApertura", "asc")->get();
	  
	  

	        //$query = $this->db->select('dataApertura , Modello')->from('oggetti')->order_by("dataApertura", "asc")->like('dataApertura',"$year-$month")->get();
			
			
	  $query = $query->result();

 foreach ($query as $row){
		 
		 $day = (int)substr($row->dataApertura,8,2);
		 $hour = date("H:i",strtotime($row->dataApertura));
		 
		 if ($day != $temp) {
		 
		 if($row->status == '1') {$events[(int)$day] = "</br><i class='fa fa-circle' style='color:#3dc45b' aria-hidden='true'></i> <span style='color:#000;'>"."<a data-dismiss='modal' class='visualizza_or' href='#visualizza_ordini' data-toggle='modal' data-num=".$row->ID.">".$row->Modello." ".$row->ID."</a><div style='float:right;color:#667fa0'>".$hour."</div></span>"; } else  $events[(int)$day] = "<i class='fa fa-circle' style='color:#d0d419' aria-hidden='true'></i> "."<a data-dismiss='modal' class='visualizza_or' href='#visualizza_ordini' data-toggle='modal' data-num=".$row->ID.">".$row->Modello." ".$row->ID."</a><div style='float:right;color:#667fa0'>".$hour."</div>";
		
		//$events[(int)$day] = $row->Modello;
		 
		 }
			
  
			if ($day == $temp) { 
			 
		
		if($row->status == '1') {$events[(int)$day] .= "</br><i class='fa fa-circle' style='color:#3dc45b' aria-hidden='true'></i> <span style='color:#000'>"."<a data-dismiss='modal' class='visualizza_or' href='#visualizza_ordini' data-toggle='modal' data-num=".$row->ID.">".$row->Modello." ".$row->ID."</a><div style='float:right;color:#667fa0'>".$hour."</div></span>"; } else 	$events[(int)$day] .= "</br><i class='fa fa-circle' style='color:#d0d419' aria-hidden='true'></i> "."<a data-dismiss='modal' class='visualizza_or' href='#visualizza_ordini' data-toggle='modal' data-num=".$row->ID.">".$row->Modello." ".$row->ID."</a><div style='float:right;color:#667fa0'>".$hour."</div>";
			
			 
			}
		
				$temp = $day;
				
       						  }
							  
 		  
 					   

      return $events;

   }

}